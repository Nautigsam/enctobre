import { useEffect, useState } from "preact/hooks";

type LoginFormProps = {
  formId: string;
  csrfToken: string;
};

export default function LoginForm(props: LoginFormProps) {
  const { csrfToken, formId } = props;

  const [loading, setLoading] = useState(true);
  const [loginError, setLoginError] = useState("");

  useEffect(function () {
    // @ts-ignore
    globalThis.getRedirectResult(globalThis.Auth).then((result) => {
      if (result === null) {
        setLoading(false);
        return;
      }
      return result.user.getIdToken().then((accessToken: string) => {
        return loginAndRedirect(accessToken, () => {
          setLoading(false);
        });
      });
    }).catch(() => {
      setLoading(false);
      setLoginError("Erreur d'authentification");
    });
  }, []);

  async function onClickGoogle() {
    // @ts-ignore
    const provider = new globalThis.GoogleAuthProvider();
    // @ts-ignore
    await globalThis.signInWithRedirect(
      // @ts-ignore
      globalThis.Auth,
      provider,
    );
  }

  async function onClickTwitter() {
    // @ts-ignore
    const provider = new globalThis.TwitterAuthProvider();
    // @ts-ignore
    await globalThis.signInWithRedirect(
      // @ts-ignore
      globalThis.Auth,
      provider,
    );
  }

  async function onClickMicrosoft() {
    // @ts-ignore
    const provider = new globalThis.OAuthProvider("microsoft.com");
    // @ts-ignore
    await globalThis.signInWithRedirect(
      // @ts-ignore
      globalThis.Auth,
      provider,
    );
  }

  function onSubmit(e: Event) {
    e.preventDefault();

    const formData = new FormData(e.target as HTMLFormElement);
    const email = formData.get("email")?.valueOf() as string;
    const password = formData.get("password")?.valueOf() as string;

    setLoading(true);
    // @ts-ignore
    globalThis.signInWithEmailAndPassword(globalThis.Auth, email, password)
      .then(
        // @ts-ignore
        ({ user }) => {
          return loginAndRedirect(user.accessToken, () => {
            setLoading(false);
          });
        },
      )
      .catch((err: Error & { code?: string }) => {
        setLoading(false);
        if (!err.code) {
          setLoginError(`Auth error (${err.name})`);
          return;
        }
        if (
          ["auth/invalid-email", "auth/wrong-password"].includes(
            err.code,
          )
        ) {
          setLoginError("Identifiants incorrects");
        } else {
          setLoginError(`Auth error (${err.code})`);
        }
      });
  }

  function loginAndRedirect(
    idToken: string,
    onFetchResponse: () => void = () => {},
  ) {
    return fetch("/login", {
      method: "POST",
      body: JSON.stringify({ idToken }),
      headers: {
        "Content-Type": "application/json",
        "Form-Id": formId,
        "Csrf-Token": csrfToken,
      },
    }).then(() => {
      onFetchResponse();
      window.location.assign("/");
    });
  }

  return (
    <div>
      <div
        class={`${
          loading ? "" : "hidden"
        } absolute left-0 top-0 right-0 bottom-0 w-screen h-screen bg-gray-800 opacity-80`}
      >
      </div>
      <div class="flex flex-col gap-5 md:w-5/12 md:m-auto">
        {loginError
          ? (
            <div class="border-2 border-red-500 rounded p-2 font-bold text-sm bg-red-300 text-black">
              {loginError}
            </div>
          )
          : ""}
        <button
          onClick={onClickGoogle}
          class="border rounded p-2 uppercase font-bold hover:bg-gray-200 dark:hover:bg-gray-800"
        >
          Avec un compte Google
        </button>
        <button
          onClick={onClickTwitter}
          class="border rounded p-2 uppercase font-bold hover:bg-gray-200 dark:hover:bg-gray-800"
        >
          Avec un compte Twitter
        </button>
        <button
          onClick={onClickMicrosoft}
          class="border rounded p-2 uppercase font-bold hover:bg-gray-200 dark:hover:bg-gray-800"
        >
          Avec un compte Microsoft
        </button>
        <form onSubmit={onSubmit} class="border rounded p-2">
          <h2 class="uppercase font-bold text-center">Avec un email</h2>
          <div class="mt-2 flex flex-col gap-2">
            <label for="login-email" class="uppercase">Email</label>
            <input
              type="text"
              name="email"
              id="login-email"
              class="p-2 bg-gray-200 dark:bg-gray-700"
            />
            <label for="login-password" class="uppercase">
              Mot de passe
            </label>
            <input
              type="password"
              name="password"
              id="login-password"
              class="p-2 bg-gray-200 dark:bg-gray-700"
            />
            <button
              type="submit"
              class="mt-2 rounded p-2 uppercase font-bold bg-green-700 text-white"
            >
              Se connecter
            </button>
          </div>
          <p class="mt-2 mx-auto">
            <a href="/signup" class="block w-full text-center">
              Pas encore de compte ?
            </a>
          </p>
        </form>
      </div>
    </div>
  );
}
