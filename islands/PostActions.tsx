import { useState } from "preact/hooks";
import { PostsGridItem } from "../components/PostsGrid.tsx";

type PostActionsProps = {
  post: PostsGridItem;
  canRemove: boolean;
  canLike: boolean;
};

function SolidHeart({ className = "" } = {}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="currentColor"
      className={`w-6 h-6 ${className}`}
      aria-label="Unlike button icon"
    >
      <path d="M11.645 20.91l-.007-.003-.022-.012a15.247 15.247 0 01-.383-.218 25.18 25.18 0 01-4.244-3.17C4.688 15.36 2.25 12.174 2.25 8.25 2.25 5.322 4.714 3 7.688 3A5.5 5.5 0 0112 5.052 5.5 5.5 0 0116.313 3c2.973 0 5.437 2.322 5.437 5.25 0 3.925-2.438 7.111-4.739 9.256a25.175 25.175 0 01-4.244 3.17 15.247 15.247 0 01-.383.219l-.022.012-.007.004-.003.001a.752.752 0 01-.704 0l-.003-.001z" />
    </svg>
  );
}
function OutlineHeart({ className = "" } = {}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      className={`w-6 h-6 ${className}`}
      aria-label="Like button icon"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
      />
    </svg>
  );
}

export default function PostActions(
  { post, canRemove, canLike }: PostActionsProps,
) {
  const [likeState, setLikeState] = useState({
    isLiked: post.isLiked,
    likesCount: post.likesCount,
  });

  async function onLikeButtonClick() {
    const previousState = likeState;
    setLikeState((state) => ({
      isLiked: !state.isLiked,
      likesCount: state.isLiked ? state.likesCount - 1 : state.likesCount + 1,
    }));
    const res = await fetch(`/api/posts/${post.name}?action=togglelike`, {
      method: "post",
    });
    if (res.status !== 200) {
      setLikeState(previousState);
    }
  }

  async function onDeleteButtonClick() {
    const res = await fetch(`/api/posts/${post.name}`, { method: "delete" });
    if (res.status === 200) {
      window.location.reload();
    }
  }

  function getLikeComponent() {
    return (
      <span class="flex flex-row gap-2">
        {likeState.isLiked
          ? <SolidHeart className="text-red-500" />
          : <OutlineHeart />}
        {` ${likeState.likesCount}`}
      </span>
    );
  }

  return (
    <div class="flex flex-row gap-2">
      {canLike
        ? (
          <button onClick={onLikeButtonClick} class="focus:outline-none">
            {getLikeComponent()}
          </button>
        )
        : getLikeComponent()}
      {canRemove
        ? (
          <button
            onClick={onDeleteButtonClick}
            class="border rounded p-1"
          >
            &#128465;
          </button>
        )
        : ""}
    </div>
  );
}
