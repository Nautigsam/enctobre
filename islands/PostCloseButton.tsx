export default function PostCloseButton() {
  function onLinkClick() {
    console.log(window.history.length);
    if (window.history.length > 2) {
      window.history.back();
    } else {
      window.location.assign("/");
    }
  }

  return (
    <div
      class="absolute right-0 bottom-0 p-2 m-5 border rounded-full select-none md:right-auto md:bottom-auto md:left-0 md:top-0"
    >
      <button
        onClick={onLinkClick}
        class="block static text-l font-bold text-center object-center"
      >
        Retour
      </button>
    </div>
  );
}
