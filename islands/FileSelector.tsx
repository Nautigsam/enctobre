import { Fragment } from "preact";
import { useState } from "preact/hooks";

type FileSelectorProps = {
  inputName: string;
  inputId: string;
};

export default function FileSelector(props: FileSelectorProps) {
  const { inputName, inputId } = props;

  const [selectedFile, setSelectedFile] = useState("");

  function onInputChange(e: Event) {
    const files = (e.target as HTMLInputElement).files;
    setSelectedFile(files?.item(0)?.name ?? "");
  }

  return (
    <Fragment>
      <label
        for={inputId}
        // NOTE(abertron): I don't know why but it seems we have to set
        // the same properties on both the label AND the hidden input element.
        class={`border-dashed border-2 border-green-700 rounded-lg uppercase text-center p-5 md:col-start-2 md:col-span-4`}
      >
        {selectedFile
          ? `Image sélectionnée: ${selectedFile}`
          : "Sélectionner une image"}
      </label>
      <input
        type="file"
        name={inputName}
        id={inputId}
        required
        accept="image/*"
        onChange={onInputChange}
        class="hidden border-dashed border-2 border-green-700 rounded-lg text-center p-5 md:col-start-2 md:col-span-4"
      />
    </Fragment>
  );
}
