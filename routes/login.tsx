import { Handlers, PageProps } from "$fresh/server.ts";
import LoginForm from "../islands/LoginForm.tsx";
import { encode } from "@std/encoding/base32.ts";
import { createSessionCookie } from "../firebase-server.ts";
import PageState from "../utils/page-state.ts";

const DevMode = Deno.env.get("ENC22_DEV") === "true";

type LoginPostBody = {
  idToken: string;
};

type LoginProps = {
  formId: string;
  csrfToken: string;
};

export const handler: Handlers<unknown, PageState> = {
  GET(_req, ctx) {
    ctx.state.title = "Connexion";
    ctx.state.showNavbarActions = false;

    const formIdRaw = new Uint8Array(20);
    crypto.getRandomValues(formIdRaw);
    const formId = encode(formIdRaw);

    const csrfTokenRaw = new Uint8Array(20);
    crypto.getRandomValues(csrfTokenRaw);
    const csrfToken = encode(csrfTokenRaw);

    sessionStorage.setItem(formId, csrfToken);
    setTimeout(() => {
      sessionStorage.removeItem(formId);
    }, 5 * 60 * 1000);

    return ctx.render({ formId, csrfToken });
  },
  async POST(req) {
    const formId = req.headers.get("Form-Id");
    const csrfToken = req.headers.get("Csrf-Token");
    if (!formId || !csrfToken) {
      return new Response("CSRF check error", { status: 403 });
    }

    const storedCsrfToken = sessionStorage.getItem(formId);
    if (csrfToken !== storedCsrfToken) {
      return new Response("CSRF check error", { status: 403 });
    }

    const { idToken } = await req.json() as LoginPostBody;

    const validDurationSec = 60 * 60 * 24 * 5;
    const sessionCookie = await createSessionCookie(idToken, validDurationSec);
    if (!sessionCookie) {
      return new Response("Could not create session", { status: 500 });
    }

    return new Response(null, {
      status: 200,
      headers: {
        "Set-Cookie":
          `session=${sessionCookie}; Max-Age=${validDurationSec}; HttpOnly; SameSite=Strict${
            DevMode ? "" : "; Secure"
          }`,
      },
    });
  },
};

export default function Login({ data }: PageProps<LoginProps>) {
  const { csrfToken, formId } = data;
  return (
    <>
      <h1 class="my-5 text-xl md:text-2xl font-bold">
        Se connecter
      </h1>
      <LoginForm formId={formId} csrfToken={csrfToken} />
    </>
  );
}
