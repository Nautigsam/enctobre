import { LayoutProps } from "$fresh/server.ts";
import { NavBar } from "../components/Navbar.tsx";
import PageState from "../utils/page-state.ts";

export default function Layout(
  { Component, state }: LayoutProps<unknown, PageState>,
) {
  // do something with state here
  return (
    <div class="p-2 mx-auto md:px-0 md:w-9/12">
      <NavBar showActions={state.showNavbarActions ?? true} />
      <Component />
    </div>
  );
}
