import { Handlers, PageProps } from "$fresh/server.ts";
import {
  createPost,
  getAllThemesAsList,
  Post,
} from "../../db/repositories/mod.ts";
import PageState from "../../utils/page-state.ts";
import { Generator } from "ulid";
import FileSelector from "../../islands/FileSelector.tsx";
import { convertImage, freeImage } from "../../utils/image-conversion.ts";
import { ImagesStore } from "../../utils/images-store.ts";

const IdGen = new Generator();

type NewPostData = {
  themes: { value: number; label: string }[];
};

export const handler: Handlers<NewPostData | null, PageState> = {
  async GET(_req: Request, ctx) {
    if (!ctx.state.user) {
      return new Response(null, { status: 302, headers: { "Location": "/" } });
    }

    ctx.state.title = "Nouvelle image";
    ctx.state.showNavbarActions = false;

    const themes = (await getAllThemesAsList()).map((t) => ({
      value: t.day,
      label: `${t.day} - ${t.name}`,
    }));
    const data: NewPostData = { themes };
    return ctx.render(data);
  },

  async POST(req, ctx) {
    const formData = await req.formData();

    const user = ctx.state.user;
    if (!user) {
      return new Response(null, { status: 401 });
    }
    const postAuthorId = user.uid;
    const postName = IdGen.ulid_encoded()?.toString();
    if (!postName) {
      return new Response("Could not generate ulid for new post", {
        status: 500,
      });
    }

    const dayStr = formData.get("day")?.valueOf() as string | undefined;
    if (!dayStr) {
      return new Response("Missing day", { status: 403 });
    }
    const day = parseInt(dayStr);
    if (isNaN(day)) {
      return new Response("Invalid day", { status: 403 });
    }

    const imageRaw = formData.get("image")?.valueOf() as File | undefined;
    if (!imageRaw) {
      return new Response("Missing image", { status: 403 });
    }

    const imageName = `${postName}.jpg`;

    try {
      const image = new Uint8Array(await imageRaw.arrayBuffer());

      const converted = await convertImage(image);
      await ImagesStore.set("originals", imageName, converted);
      freeImage(converted);

      const resized = await convertImage(image, true);
      await ImagesStore.set("cropped", imageName, resized);
      freeImage(resized);
    } catch (err) {
      try {
        await ImagesStore.remove("originals", imageName);
        await ImagesStore.remove("cropped", imageName);
        // deno-lint-ignore no-empty
      } catch {}
      return new Response(`Error writing file: ${err.message}`, {
        status: 500,
      });
    }

    try {
      await createPost({ author_id: postAuthorId, day, name: postName });
    } catch (err) {
      await ImagesStore.remove("originals", imageName);
      await ImagesStore.remove("cropped", imageName);
      return new Response(`Error creating post`, { status: 500 });
    }

    return new Response(null, {
      status: 303,
      headers: { "Location": `/users/${postAuthorId}` },
    });
  },
};

export default function NewPost({ data }: PageProps<NewPostData>) {
  const { themes } = data;
  // TODO(abertron): add "Cancel" button?
  return (
    <form method="post" encType="multipart/form-data" class="mt-10">
      <div class="grid grid-cols-1 items-center gap-5 md:grid-cols-6">
        <FileSelector inputName="image" inputId="post-image" />
        <label for="post-day" class="uppercase font-bold col-start-1">
          Jour
        </label>
        <select
          name="day"
          id="post-day"
          required
          class="h-10 md:col-span-5 bg-gray-200 dark:bg-gray-700"
        >
          {themes.map((t) => (
            <option label={t.label} value={t.value}>
            </option>
          ))}
        </select>
        <button
          type="submit"
          class="block rounded mt-3 p-2 uppercase font-bold text-white bg-green-700 md:col-start-3 md:col-span-2"
        >
          Valider
        </button>
      </div>
    </form>
  );
}
