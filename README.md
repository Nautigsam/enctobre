# enctobre

## Local dev server

```
ENC22_TEST_UID=<uid> ENC22_TEST_NAME=dabou deno task start
```

## Local dev server with firebase image hosting and Deno KV database

```
DENO_KV_ACCESS_TOKEN=<deno deploy token> \
ENC22_DENO_KV_UUID=<database uuid> \
ENC22_FIREBASE_BUCKET=<bucket> \
ENC22_TEST_UID=<firebase uid> \
ENC22_TEST_NAME=<name> \
deno task start
```

## Running in prod

Use these environment variables:
* DENO_KV_ACCESS_TOKEN
* ENC22_DENO_KV_UUID
* ENC22_FIREBASE_BUCKET
* ENC22_FIREBASE_PROJECT_ID
* ENC22_FIREBASE_PRIVATE_KEY
* ENC22_FIREBASE_CLIENT_EMAIL