FROM docker.io/dpokidov/imagemagick:latest-ubuntu as imagemagick

FROM docker.io/library/rust as build-rust
RUN apt-get update && apt-get install clang -y --no-install-recommends
COPY --from=imagemagick /usr/local/include /usr/local/include/
COPY --from=imagemagick /usr/local/lib /usr/local/lib/
COPY rust/image-conversion /image-conversion
ENV CARGO_HOME=/rust
ENV CARGO_TARGET_DIR=/target
RUN cargo build --release --manifest-path=/image-conversion/Cargo.toml

FROM docker.io/denoland/deno:ubuntu-1.36.4
ARG UID=1000
ARG GID=1000
COPY --from=imagemagick /usr/local/include /usr/local/include/
COPY --from=imagemagick /usr/local/lib /usr/local/lib/
RUN ldconfig && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        libde265-0 libjpeg62 libjpeg-turbo8 libtool libx265-179 libomp5 \
        libpng16-16 ghostscript libxml2 liblcms2-2 libxext6 \
        && \
    rm -rf /var/lib/apt/lists/*
RUN groupmod -g ${GID} deno && \
    usermod -u ${UID} deno && \
    chown -R deno:deno /deno-dir
RUN mkdir -p /home/deno/app && \
    chown -R deno:deno /home/deno
WORKDIR /home/deno/app
USER deno
COPY . .
COPY --from=build-rust /target/release/libimage_conversion.so utils/
RUN deno cache main.ts

EXPOSE 8000
ENTRYPOINT ["deno", "run", "--unstable", "-A", "main.ts"]