import { Kv } from "../client.ts";

export type Post = { day: number; name: string; author_id: string };

export async function getAllPosts() {
  const posts: Post[] = [];
  for await (const post of Kv.list<Post>({ prefix: ["posts"] })) {
    posts.push(post.value);
  }
  return posts;
}

export async function getPostByName(name: string) {
  const post = await Kv.get<Post>(["posts", name]);
  return post.value;
}

export function deletePost(name: string) {
  return Kv.delete(["posts", name]);
}

export function createPost(post: Post) {
  return Kv.set(["posts", post.name], post);
}
