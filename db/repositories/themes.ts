import { Kv } from "../client.ts";

type Theme = { day: number; name: string };

function getEntries() {
  return Kv.list<Theme>({ prefix: ["themes"] });
}

export async function getAllThemesAsDict() {
  const dict: Record<number, string> = {};
  for await (const theme of getEntries()) {
    dict[theme.value.day] = theme.value.name;
  }
  return dict;
}

export async function getAllThemesAsList() {
  const list: Theme[] = [];
  for await (const theme of getEntries()) {
    list.push(theme.value);
  }
  return list.sort((e1, e2) => e1.day - e2.day);
}
