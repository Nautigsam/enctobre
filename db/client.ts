import { setDevData } from "./dev/dev_migrations.ts";

const themes = [
  "Trésor",
  "Roue",
  "Dru",
  "Volant",
  "Couverture",
  "Déchainé",
  "Répéter",
  "Feutré",
  "Danse",
  "Décalé",
  "Caméléon",
  "Flamber",
  "Bulle",
  "Risqué",
  "Monument",
  "Souvenir",
  "Velouté",
  "Plateau",
  "Fantastique",
  "Projeter",
  "Inséparable",
  "Pointu",
  "Instrument",
  "Frémir",
  "Ménage",
  "Réveil",
  "Nœud",
  "Infernal",
  "Chouchou",
  "Accident",
  "Panaché",
];

const DenoKvUuid = Deno.env.get("ENC22_DENO_KV_UUID");

if (!DenoKvUuid) {
  console.error(
    "Need variables ENC22_DENO_KV_UUID and DENO_KV_ACCESS_TOKEN",
  );
  Deno.exit(1);
}

const Kv = await Deno.openKv(
  `https://api.deno.com/databases/${DenoKvUuid}/connect`,
);

await Promise.all(themes.map((name, i) => {
  const day = i + 1;
  return Kv.set(["themes", i + 1], { day, name });
}));

const DevMode = Deno.env.get("ENC22_DEV") === "true";

if (DevMode) {
  console.log("Setting development data...");
  await setDevData(Kv);
}

export { Kv };
