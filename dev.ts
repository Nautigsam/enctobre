#!/usr/bin/env -S deno run -A --watch=static/,routes/

import dev from "$fresh/dev.ts";
import config from "./fresh.config.ts";

Deno.env.set("ENC22_DEV", "true");

const installSamples = async (srcdir: URL, dstdir: URL) => {
  for await (
    const entry of Deno.readDir(srcdir)
  ) {
    if (!entry.isFile) continue;
    await Deno.copyFile(
      new URL(entry.name, srcdir),
      new URL(entry.name, dstdir),
    );
  }
};
await installSamples(
  new URL("samples/originals/", import.meta.url),
  new URL("images/originals/", import.meta.url),
);
await installSamples(
  new URL("samples/cropped/", import.meta.url),
  new URL("images/cropped/", import.meta.url),
);

await dev(import.meta.url, "./main.ts", config);
