use magick_rust::{bindings, magick_wand_genesis, MagickError, MagickWand};
use std::slice;
use std::sync::Once;

static START: Once = Once::new();

#[no_mangle]
pub extern "C" fn convert_image(
    src_data: *const u8,
    src_size: usize,
    resize: u8,
    dst_data_ptr: *mut *const u8,
) -> u32 {
    let src = unsafe { slice::from_raw_parts(src_data, src_size) };

    let mut output = Vec::with_capacity(0);

    START.call_once(|| {
        magick_wand_genesis();
    });

    let mut wand = MagickWand::new();

    let res = match wand.read_image_blob(src) {
        Ok(()) => match process_image(&mut wand, &mut output, resize != 0) {
            Ok(()) => Ok(()),
            Err(err) => Err(err),
        },
        Err(err) => Err(err),
    };
    let slice_res = match res {
        Ok(()) => {
            if output.len() > u32::MAX.try_into().unwrap() {
                Err("Too big output image".into())
            } else {
                Ok(output.as_slice())
            }
        }
        Err(err) => Err(err),
    };

    let output_slice = match slice_res {
        Ok(slice) => slice,
        Err(_err) => {
            unsafe { *dst_data_ptr = std::ptr::null() };
            return 0;
        }
    };

    // Clone the data to allow dropping input and output without losing the data (which we return to caller)
    let mut dst = vec![0; output_slice.len()];
    dst.clone_from_slice(output_slice);

    unsafe { *dst_data_ptr = dst.as_ptr() };
    let dst_len = dst.len();

    std::mem::forget(dst);

    dst_len.try_into().unwrap()
}

#[no_mangle]
pub extern "C" fn drop_image(data_ptr: *const u8, len: usize) {
    let data: Vec<u8> = unsafe { Vec::from_raw_parts(data_ptr as *mut u8, len, len) };
    drop(data);
}

fn process_image(
    mw: &mut MagickWand,
    output: &mut Vec<u8>,
    resize: bool,
) -> Result<(), MagickError> {
    match mw.auto_orient() {
        true => Ok::<(), MagickError>(()),
        false => Err("Could not auto orient image".into()),
    }?;

    if resize {
        handle_crop(mw)?;
        let format = mw.get_image_format()?;
        if format == "SVG" || format == "MVG" {
            return Err("Unsupported format".into());
        }

        let (width, height) = compute_output_size(mw);
        mw.resize_image(
            width as usize,
            height as usize,
            bindings::FilterType_LanczosFilter,
        );
    }

    mw.set_image_format("JPEG")?;

    let mut temp = mw.write_image_blob("JPEG")?;
    output.append(&mut temp);

    Ok(())
}

fn handle_crop(mw: &MagickWand) -> Result<(), MagickError> {
    let r = 4.0 / 3.0;

    let original_width = mw.get_image_width();
    let original_height = mw.get_image_height();

    let original_width_f64 = original_width as f64;
    let original_height_f64 = original_height as f64;

    let ratio = original_width_f64 / original_height_f64;

    let (new_width, new_height) = if r >= ratio {
        (original_width, (original_width_f64 / r).round() as usize)
    } else {
        ((original_height_f64 * r).round() as usize, original_height)
    };

    let x = (original_width - new_width) / 2;
    let y = (original_height - new_height) / 2;

    mw.crop_image(new_width, new_height, x as isize, y as isize)?;

    Ok(())
}

fn compute_output_size(mw: &MagickWand) -> (u16, u16) {
    let input_width = mw.get_image_width() as u16;
    let input_height = mw.get_image_height() as u16;
    let mut width: u16 = 400;
    let mut height: u16 = input_height;

    if width == input_width {
        return (width, height);
    } else if width > input_width {
        width = input_width;
    }

    let input_width_f64 = f64::from(input_width);
    let input_height_f64 = f64::from(input_height);
    let width_f64 = f64::from(width);
    let height_f64 = f64::from(height);

    let ratio = input_width_f64 / input_height_f64;

    let wr = input_width_f64 / width_f64;
    let hr = input_height_f64 / height_f64;

    if wr >= hr {
        height = (width_f64 / ratio).round() as u16;
    } else {
        width = (height_f64 * ratio).round() as u16;
    }

    (width, height)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::{Read, Write};

    #[test]
    fn it_works() {
        let current_dir = std::env::current_dir().unwrap();
        let src_path = current_dir.as_path().join("spacex.jpg");
        assert!(src_path.exists());

        let mut src = File::options().read(true).open(src_path).unwrap();
        let mut src_vec = Vec::<u8>::new();
        src.read_to_end(&mut src_vec).unwrap();

        let mut dst_data_ptr: *const u8 = std::ptr::null();
        let dst_len: usize = convert_image(src_vec.as_ptr(), src_vec.len(), 1, &mut dst_data_ptr)
            .try_into()
            .unwrap();
        assert_ne!(dst_len, 0);
        assert_ne!(dst_data_ptr, std::ptr::null());

        let dst_data = unsafe { slice::from_raw_parts(dst_data_ptr, dst_len) };
        assert_eq!(dst_data.len(), dst_len);

        let dst_path = current_dir.as_path().join("spacex-resized.jpg");
        let mut dst_file = File::options()
            .create(true)
            .write(true)
            .open(dst_path)
            .unwrap();

        let written = dst_file.write(dst_data).unwrap();
        assert_ne!(written, 0);

        drop_image(dst_data_ptr, dst_len);
    }
}
