import { useContext } from "preact/hooks";
import { UserContext } from "../utils/user-context.tsx";
import NavBarActions from "../islands/NavBarActions.tsx";

type NavBarProps = {
  showActions?: boolean;
};

export function NavBar(props: NavBarProps) {
  const { showActions = true } = props;
  const user = useContext(UserContext);

  return (
    <nav class="pr-2 py-1 border rounded bg-gray-100 dark:bg-black md:px-3 md:py-3">
      <div
        class={`flex justify-between mx-auto items-center`}
      >
        <a href="/" class="rounded p-2 flex flex-row items-end">
          <img
            src="/logo.png"
            alt="Enctobre logo"
            width="40"
          />
          nctobre 2023
        </a>
        {showActions
          ? (
            <NavBarActions
              user={user}
            />
          )
          : ""}
      </div>
    </nav>
  );
}
