import { ClientSQLite, NessieConfig } from "nessie";

const DatabaseUri = Deno.env.get("ENC22_DB") ?? ":memory:";

const client = new ClientSQLite(DatabaseUri);

/** This is the final config object */
const config: NessieConfig = {
  client,
  migrationFolders: [new URL("db/migrations", import.meta.url).pathname],
  seedFolders: [new URL("db/seeds", import.meta.url).pathname],
};

export default config;
