import { ComponentChildren, createContext } from "preact";
import { User } from "../db/repositories/mod.ts";

export const UserContext = createContext<User | null>(null);

type UserContextProviderProps = {
  value: User | null;
  children: ComponentChildren;
};

export default function UserContextProvider(
  { value, children }: UserContextProviderProps,
) {
  return (
    <UserContext.Provider value={value}>
      {children}
    </UserContext.Provider>
  );
}
