import { Plugin } from "$fresh/server.ts";

export default function firebase(): Plugin {
  const main =
    `data:application/javascript,import { initializeApp } from "https://cdn.skypack.dev/firebase@9.9.4/app?dist=es2020";
import {
  createUserWithEmailAndPassword,
  getAuth,
  getRedirectResult,
  GoogleAuthProvider,
  inMemoryPersistence,
  OAuthProvider,
  signInWithEmailAndPassword,
  signInWithPopup,
  signInWithRedirect,
  TwitterAuthProvider,
} from "https://cdn.skypack.dev/firebase@9.9.4/auth?dist=es2020";
export default async function () {
  globalThis.createUserWithEmailAndPassword = createUserWithEmailAndPassword;
  globalThis.getRedirectResult = getRedirectResult;
  globalThis.GoogleAuthProvider = GoogleAuthProvider;
  globalThis.OAuthProvider = OAuthProvider;
  globalThis.signInWithEmailAndPassword = signInWithEmailAndPassword;
  globalThis.signInWithPopup = signInWithPopup;
  globalThis.signInWithRedirect = signInWithRedirect;
  globalThis.TwitterAuthProvider = TwitterAuthProvider;

  const firebaseConfig = {
    apiKey: "AIzaSyA5qiHfmnA9LAof1XjdEha1qv6jyC4FgIs",
    authDomain: "enctobre2022.firebaseapp.com",
    projectId: "enctobre2022",
    storageBucket: "enctobre2022.appspot.com",
    messagingSenderId: "477619007873",
    appId: "1:477619007873:web:50c3b6160830acf8323880"
  };

  globalThis.App = initializeApp(firebaseConfig);
  globalThis.Auth = getAuth(globalThis.App);

  globalThis.Auth.languageCode = 'fr';
  await globalThis.Auth.setPersistence(inMemoryPersistence);
};`;
  return {
    name: "firebase",
    entrypoints: { "main": main },
    render(ctx) {
      ctx.render();
      return {
        scripts: [{ entrypoint: "main", state: {} }],
      };
    },
  };
}
